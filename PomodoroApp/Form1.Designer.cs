﻿namespace PomodoroApp {
    partial class frmMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.lblRad = new System.Windows.Forms.Label();
            this.lblOdmor = new System.Windows.Forms.Label();
            this.lblVrijeme = new System.Windows.Forms.Label();
            this.tbRad = new System.Windows.Forms.TextBox();
            this.tbOdmor = new System.Windows.Forms.TextBox();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.tmrOdbrojavanje = new System.Windows.Forms.Timer(this.components);
            this.lblStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblRad
            // 
            this.lblRad.AutoSize = true;
            this.lblRad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblRad.Location = new System.Drawing.Point(135, 34);
            this.lblRad.Name = "lblRad";
            this.lblRad.Size = new System.Drawing.Size(39, 20);
            this.lblRad.TabIndex = 0;
            this.lblRad.Text = "Rad";
            // 
            // lblOdmor
            // 
            this.lblOdmor.AutoSize = true;
            this.lblOdmor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblOdmor.Location = new System.Drawing.Point(405, 34);
            this.lblOdmor.Name = "lblOdmor";
            this.lblOdmor.Size = new System.Drawing.Size(57, 20);
            this.lblOdmor.TabIndex = 1;
            this.lblOdmor.Text = "Odmor";
            // 
            // lblVrijeme
            // 
            this.lblVrijeme.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVrijeme.Font = new System.Drawing.Font("Hack", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblVrijeme.Location = new System.Drawing.Point(75, 112);
            this.lblVrijeme.Name = "lblVrijeme";
            this.lblVrijeme.Size = new System.Drawing.Size(440, 79);
            this.lblVrijeme.TabIndex = 2;
            this.lblVrijeme.Text = "25:00";
            this.lblVrijeme.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbRad
            // 
            this.tbRad.Location = new System.Drawing.Point(75, 60);
            this.tbRad.Name = "tbRad";
            this.tbRad.Size = new System.Drawing.Size(165, 20);
            this.tbRad.TabIndex = 3;
            this.tbRad.Text = "25";
            // 
            // tbOdmor
            // 
            this.tbOdmor.Location = new System.Drawing.Point(342, 60);
            this.tbOdmor.Name = "tbOdmor";
            this.tbOdmor.Size = new System.Drawing.Size(173, 20);
            this.tbOdmor.TabIndex = 4;
            this.tbOdmor.Text = "5";
            // 
            // btnStartStop
            // 
            this.btnStartStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnStartStop.Location = new System.Drawing.Point(75, 230);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(165, 58);
            this.btnStartStop.TabIndex = 5;
            this.btnStartStop.Text = "Start / Stop";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnReset.Location = new System.Drawing.Point(342, 230);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(173, 58);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // tmrOdbrojavanje
            // 
            this.tmrOdbrojavanje.Interval = 1000;
            this.tmrOdbrojavanje.Tick += new System.EventHandler(this.tmrOdbrojavanje_Tick);
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(75, 96);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(440, 23);
            this.lblStatus.TabIndex = 7;
            this.lblStatus.Text = "RAD";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 339);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnStartStop);
            this.Controls.Add(this.tbOdmor);
            this.Controls.Add(this.tbRad);
            this.Controls.Add(this.lblVrijeme);
            this.Controls.Add(this.lblOdmor);
            this.Controls.Add(this.lblRad);
            this.Name = "frmMain";
            this.Text = "PomodoroApp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRad;
        private System.Windows.Forms.Label lblOdmor;
        private System.Windows.Forms.Label lblVrijeme;
        private System.Windows.Forms.TextBox tbRad;
        private System.Windows.Forms.TextBox tbOdmor;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Timer tmrOdbrojavanje;
        private System.Windows.Forms.Label lblStatus;
    }
}

