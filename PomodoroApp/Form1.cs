﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PomodoroApp {
    public partial class frmMain : Form {
        int trenutniBrojSekundi;
        bool radPokrenut;

        private void azurirajPrikaz() {
            // labal postavljen na sredinu forme neka prikazuje trenutno stanje sekundi, no u obliku MM:SS
            // koristi se string.Format kako bi se prikazale minute i sekunde s vodecim nulama
            // vise o tome na: https://docs.microsoft.com/en-us/dotnet/api/system.string.format?view=netframework-4.8#control-formatting
            lblVrijeme.Text = string.Format("{0:D2}:{1:D2}", trenutniBrojSekundi / 60, trenutniBrojSekundi % 60);
            
            // Text je svojstvo (podatkovni clan) forme tj. prozora, svojstvo definira što će se prikazivati u naslovnoj traci prozora
            Text = string.Format("[{0:D2}:{1:D2}] PomodoroApp", trenutniBrojSekundi / 60, trenutniBrojSekundi % 60);

            if (radPokrenut) {
                lblStatus.BackColor = SystemColors.Control;
                lblStatus.Text = "RAD";
            } else {
                lblStatus.BackColor = Color.LimeGreen;
                lblStatus.Text = "ODMOR";
            }
        }

        private void azurirajTrenutanBrojSekundi() {
            // ako je pokrenut rad, trenutan broj sekundi ce biti jednak umnošku broja minuta
            // upisanom u text box za rad i 60, u protivnom, pokrenut je odmor i broj sekundi ce biti jednak
            // umnosku broja minuta upisanom u text box za odmor i 60

            // program ce "puknuti" ako u text box upišemo bilo sto sto nije moguce pretvoriti
            // u cijeli broj
            if (radPokrenut) {
                trenutniBrojSekundi = int.Parse(tbRad.Text) * 60;
            } else {
                trenutniBrojSekundi = int.Parse(tbOdmor.Text) * 60;
            }
        }

        public frmMain() {
            InitializeComponent();
            // prilikom stvaranja i prikaza forme (prozora) postaviti da je pokrenut rad jer
            // pokretanjem programa moramo poceti od rada, ne odmora
            radPokrenut = true;
            // prilikom stvaranja i prikaza forme (prozora), potrebno je azurirati
            // trenutan broj sekundi i azurirai prikaz
            azurirajTrenutanBrojSekundi();
            azurirajPrikaz();
        }

        private void btnStartStop_Click(object sender, EventArgs e) {
            // ukljucujemo timer ako je bio iskljucen, odnosno iskljucujemo ako je bio ukljucen
            tmrOdbrojavanje.Enabled = !tmrOdbrojavanje.Enabled;
            // ukoliko je pokrenut timer, iskljucujemo kontrole za unos minuta rada i odmora
            // ukoliko nije pokrenut timer, ponovo ukljucujemo kontrole kako bi se mogle izmjeniti
            // minute za rad i odmor
            if (tmrOdbrojavanje.Enabled) {
                tbRad.Enabled = false;
                tbOdmor.Enabled = false;
            } else {
                tbRad.Enabled = true;
                tbOdmor.Enabled = true;
            }
        }

        private void btnReset_Click(object sender, EventArgs e) {
            // ukoliko resetiramo broj, moramo iskljuciti timer
            tmrOdbrojavanje.Enabled = false;
            // bez obzira na stanje ukljucenosti kontrola za unos minuta rada i odmora
            // ukljucujemo ih kako bi omogucili izmjenu vrijednosti u njima
            tbRad.Enabled = true;
            tbOdmor.Enabled = true;

            // vracamo timer na rad i azuriramo broj sekundi i prikaz
            radPokrenut = true;
            azurirajTrenutanBrojSekundi();
            azurirajPrikaz();
        }

        private void tmrOdbrojavanje_Tick(object sender, EventArgs e) {
            if (trenutniBrojSekundi == 0) {
                radPokrenut = !radPokrenut;
                azurirajTrenutanBrojSekundi();
            }
            trenutniBrojSekundi--;
            azurirajPrikaz();
        }
    }
}
